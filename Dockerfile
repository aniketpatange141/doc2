FROM alpine:3.17.0
COPY code/deps /lib64/
RUN chmod 777 /lib64/*
COPY ./code/C_Code.out ./code/liblib1.so ./code/liblib2.so ./code/csv_readwrite.so /home/bin/
WORKDIR /home/bin
CMD ["./C_Code.out"]
